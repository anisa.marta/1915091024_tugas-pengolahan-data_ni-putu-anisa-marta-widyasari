<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>input</title>
  </head>
  <style>
      .col-8{
        background-color:lightblue;
      }
      .atas{
          text-align:center;
          padding: 25px;
      }
      
  </style>
  <body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8" >
            <div class="atas">
                <h3 style="font-weight:bold;">FORM PENGOLAHAN DATA</h3>
            </div>
            <form action="" method="post">
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama">
                </div>
                <div class="mb-3">
                    <label for="mapel" class="form-label">Mata Pelajaran</label>
                    <input type="text" name="mapel" class="form-control" id="mapel">
                </div>
                <div class="mb-3">
                    <label for="uts" class="form-label">Nilai UTS</label>
                    <input type="number" name="uts" class="form-control" id="uts">
                </div>
                <div class="mb-3">
                    <label for="uas" class="form-label">Nilai UAS</label>
                    <input type="number" name="uas" class="form-control" id="uas">
                </div>
                <div class="mb-3">
                    <label for="tugas" class="form-label">Nilai Tugas</label>
                    <input type="number" name="tugas" class="form-control" id="tugas">
                </div>
                <button type="submit" name="submit" class="btn btn-primary">KIRIM</button>
            </form><br>
            </div>
        </div>
    </div>
    <br>
    <br>
<?php
    if(isset($_POST["submit"])):?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8" >
            <?php
                $nama=$_POST["nama"];
                $mapel=$_POST["mapel"];
                $uts=$_POST["uts"];
                $uas=$_POST["uas"];
                $tugas=$_POST["tugas"];
                $total= ($_POST["uts"]*35/100)+($_POST["uas"]*50/100)+($_POST["tugas"]*15/100);

                if(($total)>=90 && ($total)<=100){
                    $grade="A";
                }
                if(($total)>70 && ($total)<90){
                    $grade="B";
                }
                if(($total)>50 && ($total)<70){
                    $grade="C";
                }
                if(($total)<=50){
                    $grade="D";
                }

            ?>
            <br>
            <h3 align="center" style="font-weight:bold;">HASIL PENGOLAHAN DATA</h3><br>
            <?php
                echo "Nama : $nama <br>";
                echo "Mata Pelajaran : $mapel<br>";
                echo "Nilai UTS : $uts<br>";
                echo "Nilai UAS : $uas<br>";
                echo "Nilai Tugas : $tugas<br>";
                echo "Nilai Total : $total<br><br>";
                echo "<strong>Grade Nilai : $grade</strong><br>";
            ?>
            </div>
        </div>
    </div>
<?php endif; ?>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>